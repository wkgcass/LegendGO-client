# Events

This doc records all the events

## cardCandidateCancelSelect

`card` emit with no argument

send via `broadcast`

handled in `cardCandidateCancelSelect`

## cardCandidateSelect

`card` emit with no argument

send via `broadcast`

handled in `cardCandidateSelect`

## cardMoveToPile

emit with `{card: card, pile: str(pile type)}`

send via `broadcast`

handled in `cardMoveToPile`

## showPile

`pile` emit with no argument

send via `broadcast`

handled in `showPile`

## beforeCardChooseCandidate

`cardSelectionService` emit with argument `filter {}`

send via `componentBroadcast`

handled in `card`

## afterCardChooseCandidate

`cardSelectionService` emit with no argument

send via `componentBroadcast`

handled in `card`

## requirePile

`cardMoveToPile` emit with `{type: str, side: str, cb: (pile)}`

send via `componentBroadcast`

handled in `pile`

## componentRemoved

`ComponentContainer` emit with _the component inner object_

send via `broadcast`

## hpChange

`hpSink` emit with `{side:str, value:int, cb[optional]}`

send via `componentBroadcast`

## pileCardMove

emit with `{side: str, from: str, to: str, cards:[_cards_], top: _card_[optional]}`

`from/to` could be `hand` or `pile` name

send via `componentBroadcast`

handled in `pile`
