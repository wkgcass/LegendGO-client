let utils = require('./utils');
let fs = require('fs');

let registry = {};
let images = {};
let cardDesc = {};
let initiated = false;

let configuration = module.exports;

configuration.init = function () {
    if (initiated) return;
    initiated = true;
    registry.window = {};
    registry.window.width = 2500;
    registry.window.height = 1492;

    registry.dev = {};
    registry.dev.enabled = true;

    registry.mouse = {};
    registry.mouse.lastMouseDown = {};
    registry.mouse.lastMouseDown.delay = 50;

    registry.components = {};
    registry.components.leftSideBar = {
        width: 500,
        marginRight: 240,
        paddingTop: 40,
        frame: {anglePixels: 25},
        property: {padding: 36, height: 40, font: '30px courier'},
        text: {fontSize: 34, fontFamily: '宋体', margin: 15},
        skillMarginBottom: 40,
        star: {width: 20, margin: 5}
    };
    registry.components.slot = {
        hMargin: 30,
        vMargin: 40,
        color: {r: 0, g: 0, b: 0},
        font: "50px Helvetica Helvetica",
        fontColor: '#fff'
    };
    registry.components.powerSink = {width: 46};
    registry.components.card = {
        thumb: {width: 120, height: 190, fontSize: 28},
        full: {width: 320, height: 500}
    };
    registry.components.skill = {width: 110};
    registry.components.buff = {width: 20, margin: 10};
    registry.components.desc = {width: 500};
    registry.components.testButton = {width: 150, height: 60, arc: 20};
    registry.components.pile = {drawEvery: 8, dx: 0.6, dy: 0.2};

    registry.components.sys = {};
    registry.components.sys.errorMsg = {width: 500, height: 300};

    registry.card = {
        'front': {},
        'properties': {'1': '龙'}
    };

    registry.buff = {
        'def-down': {
            desc: (info) => `防御力下降${info.value}点`
        }
    };

    registry.i18n = {};
    registry.i18n.cannotDisplayPile = '该牌堆不能被显示';
    registry.i18n.packUpPile = '收起';
    registry.i18n.remains = '剩余回合';
};

configuration.get = function (key, defaultValue) {
    if (key.startsWith('card.desc')) {
        return getCardDesc(key.substring('card.desc.'.length));
    }

    let path = key.split('.');
    let o = registry;
    for (let p of path) {
        if (o[p] || o[p] === 0) {
            o = o[p];
        } else {
            if (arguments.length > 1) {
                return defaultValue;
            }
            throw new Error(key + ' not found!');
        }
    }
    return o;
};

function buildImageSrc(key) {
    let base = process.env.HOME + '/legendgo/images'; // ~/legendgo/images
    let pre = base + '/' + key + '.';
    for (let suffix of ['png', 'jpg']) {
        if (fs.existsSync(pre + suffix)) {
            return 'file://' + pre + suffix;
        }
    }
}

function getCardDesc(key) {
    if (cardDesc[key]) return cardDesc[key];

    let desc = JSON.parse(fs.readFileSync(buildDescSrc(key), 'utf-8'));
    cardDesc[key] = desc;
    return desc;
}

function buildDescSrc(key) {
    let base = process.env.HOME + '/legendgo/desc'; // ~/legendgo/desc
    return base + '/' + key + '.json';
}

configuration.getImage = function (key, cb) {
    let url = configuration.get('images.' + key, undefined) || buildImageSrc(key);
    if (images[url]) {
        let image = images[url];
        if (image.complete) {
            cb(images[url]);
        } else {
            let f = image.onload;
            image.onload = function () {
                f();
                cb(image);
            };
        }
    } else {
        let image = new Image();
        image.src = url;
        images[url] = image;
        if (image.complete) {
            cb(image);
        } else {
            image.onload = function () {
                cb(image);
            };
            image.onerror = function () {
                utils.warn('load image failed key: ' + key + ', url: ' + url);
            };
        }
    }
};
