const electron = require('electron');
const configuration = require('./configuration');
const path = require('path');
const url = require('url');

const app = electron.app;

let mainWindow;

function createWindow() {
    configuration.init();
    let height = configuration.get('window.height');
    let width = configuration.get('window.width');
    mainWindow = new electron.BrowserWindow({
        width: 1250,
        height: 768,
        resizable: false,
        fullscreen: false
    });

    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'render-process/index.html'),
        protocol: 'file:',
        slashes: true
    }));

    if (configuration.get('dev.enabled', false)) {
        // Open the DevTools.
        mainWindow.webContents.openDevTools()
    }

    mainWindow.on('closed', function () {
        mainWindow = null;
    });
}

app.on('ready', createWindow);
app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', function () {
    if (mainWindow === null) {
        createWindow();
    }
});
