let configuration = require('../../configuration');
let utils = require('../../utils');

class PowerSink {
    constructor(type, powerType, maxPower, side) {
        this.type = type;
        this.powerType = powerType;
        this.maxPower = maxPower;
        this.side = side;
        this.power = 0;
        this.width = configuration.get('components.powerSink.width');
        this.__calculateX();
        this.__calculateY();
        this.__load();
    }

    __load() {
        configuration.getImage(this.powerType + 'Power', image => {
            this.powerImage = image;
        });
    }

    __calculateX() {
        let thumbWidth = configuration.get('components.card.thumb.width');
        let thumbHeight = configuration.get('components.card.thumb.height');
        let a = thumbWidth > thumbHeight ? thumbWidth : thumbHeight;
        let leftSideBarWidth = configuration.get('components.leftSideBar.width');
        let hMargin = configuration.get('components.slot.hMargin');
        let leftSideBarMarginRight = configuration.get('components.leftSideBar.marginRight');

        if (this.type == 'normal') {
            this.x = leftSideBarWidth + leftSideBarMarginRight + thumbWidth + (hMargin + a) * 5 +
                hMargin + thumbWidth + hMargin + this.width / 2;
        } else {
            this.x = leftSideBarWidth + leftSideBarMarginRight + thumbWidth + (hMargin + a) * 5 +
                hMargin + thumbWidth + hMargin + this.width + hMargin + this.width / 2;
        }
    }

    __calculateY() {
        let vMargin = configuration.get('components.slot.vMargin');
        let windowHeight = configuration.get('window.height');

        if (this.side == 'bottom') {
            this.y = windowHeight - 2 * vMargin - this.width * this.maxPower / 2;
        } else {
            this.y = vMargin + this.width * this.maxPower / 2;
        }
    }

    onIncreaseCrystal(sender, msg) {
        let type = msg.type;
        let side = msg.side;
        if (this.type !== type || this.side !== side) return;
        this.power += msg.count;
        if (this.power > this.maxPower) {
            this.power = this.maxPower;
        }
    }

    onDecreaseCrystal(sender, msg) {
        let type = msg.type;
        let side = msg.side;
        if (this.type !== type || this.side !== side) return;
        this.power -= msg.count;
        if (this.power < 0) {
            this.power = 0;
        }
    }

    draw(canvas) {
        let ctx = canvas.getContext('2d');
        ctx.save();
        utils.drawArcRec(ctx,
            this.x - this.width / 2, this.y - this.width * this.maxPower / 2,
            this.width, this.width * this.maxPower, 20);
        ctx.strokeStyle = '#c3a86e';
        ctx.lineWidth = 16;
        ctx.stroke();
        ctx.strokeStyle = '#c3a86e';
        ctx.lineWidth = 4;
        ctx.stroke();
        ctx.fillStyle = '#2d221a';
        ctx.fill();
        ctx.restore();
        // draw diamonds
        if (!this.powerImage) {
            // draw some circles
            for (let i = 0; i < this.power; ++i) {
                let center = {x: this.x};
                if (this.side == 'bottom') {
                    center.y = this.y + this.width * this.maxPower / 2 - (this.width / 2 + this.width * i);
                } else {
                    center.y = this.y - this.width * this.maxPower / 2 + (this.width / 2 + this.width * i);
                }
                ctx.beginPath();
                ctx.arc(center.x, center.y, this.width / 2, 0, Math.PI * 2);
                ctx.strokeStyle = 'white';
                ctx.stroke();
            }
        } else {
            // draw diamonds
            for (let i = 0; i < this.power; ++i) {
                let center = {x: this.x};
                if (this.side == 'bottom') {
                    center.y = this.y + this.width * this.maxPower / 2 - (this.width / 2 + this.width * i);
                } else {
                    center.y = this.y - this.width * this.maxPower / 2 + (this.width / 2 + this.width * i);
                }
                ctx.drawImage(this.powerImage, center.x - this.width / 2, center.y - this.width / 2, this.width, this.width);
            }
        }
    }
}

module.exports = PowerSink;
