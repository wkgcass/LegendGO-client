let configuration = require('../../configuration');
let utils = require('../../utils');
let game = require('../game');

class Buff {
    constructor(card, buffInfo, index) {
        this.card = card;
        this.buffInfo = buffInfo;
        this.index = index;
        this.a = configuration.get('components.buff.width');
        this.margin = configuration.get('components.buff.margin');
        this.y = this.a / 2 + this.margin;
        this.__calculateX();

        this.__load();
    }

    init(initConfig) {
        this.componentContainer = initConfig.componentContainer;
        this.id = initConfig.id;
        this.canvas = initConfig.canvas;
    }

    __calculateX() {
        this.x = (this.a + this.margin) * (this.index + 1) - this.a / 2;
    }

    includesPoint(x, y) {
        return x < this.x + this.a / 2 && x > this.x - this.a / 2
            && y < this.y + this.a / 2 && y > this.y - this.a / 2;
    }

    __load() {
        configuration.getImage('buff-' + this.buffInfo.type, image => {
            this.image = image;
        });
    }

    mousePos(x, y) {
        if (this.includesPoint(x, y)) {
            // skill desc
            let fontSize = configuration.get('components.leftSideBar.text.fontSize');
            let fontFamily = configuration.get('components.leftSideBar.text.fontFamily');
            let textMargin = configuration.get('components.leftSideBar.text.margin');
            let twh = new utils.TextWriterHelper(this.canvas.getContext('2d'),
                textMargin, fontSize, configuration.get('components.desc.width') - textMargin * 2,
                fontSize, fontFamily);
            let f = configuration.get('buff.' + this.buffInfo.type + '.desc');
            let desc = f(this.buffInfo);
            let lines = [desc];
            let remains = null;
            if (this.buffInfo.remains) {
                remains = configuration.get('i18n.remains') + this.buffInfo.remains;
                lines.push(remains);
            }
            let resultHeight = twh.calculateDrawLineHeight([desc]);
            game.showDescription(this.id, this.componentContainer, x, y,
                fontSize + resultHeight, (ctx, width) => {
                    // start draw
                    ctx.fillStyle = 'white';
                    let twh = new utils.TextWriterHelper(ctx,
                        textMargin, fontSize, configuration.get('components.desc.width') - textMargin * 2,
                        fontSize, fontFamily);

                    twh.writeLine(desc);
                    if (remains) {
                        twh.writeLine(remains);
                    }
                });
        }
    }

    mouseOut() {
        game.hideDescription(this.id);
    }

    draw(canvas) {
        let ctx = canvas.getContext('2d');
        if (this.image) {
            ctx.drawImage(this.image, this.x - this.a / 2, this.y - this.a / 2, this.a, this.a);
        }
        ctx.rect(this.x - this.a / 2, this.y - this.a / 2, this.a, this.a);
        ctx.strokeStyle = 'black';
        ctx.stroke();
    }
}

module.exports = Buff;
