let configuration = require('../../configuration');

class CardSlot {
    constructor(type, side, slotId) {
        this.type = type;
        this.side = side;
        this.slotId = slotId;
        let thumbWidth = configuration.get('components.card.thumb.width');
        let thumbHeight = configuration.get('components.card.thumb.height');
        this.a = thumbWidth > thumbHeight ? thumbWidth : thumbHeight;
        this.__calculateX();
        this.__calculateY();

        this.lastIn = -1;
    }

    init(args) {
        this.game = args.game;
    }

    __calculateX() {
        let thumbWidth = configuration.get('components.card.thumb.width');
        let leftSideBarWidth = configuration.get('components.leftSideBar.width');
        let hMargin = configuration.get('components.slot.hMargin');
        let leftSideBarMarginRight = configuration.get('components.leftSideBar.marginRight');
        let th;
        if (this.side == 'bottom') {
            th = this.slotId;
        } else {
            th = 4 - this.slotId;
        }
        this.x = leftSideBarWidth + leftSideBarMarginRight + thumbWidth + hMargin + th * (this.a + hMargin) + this.a / 2;
    }

    __calculateY() {
        let vMargin = configuration.get('components.slot.vMargin');
        let lineTH;
        let type = this.type;
        let side = this.side;
        if (type == 'magic') {
            lineTH = 1;
        } else if (type == 'monster') {
            lineTH = 2;
        }
        if (side == 'bottom') {
            let windowHeight = configuration.get('window.height');
            this.y = windowHeight - (this.a + vMargin) * (lineTH + 1) - vMargin + this.a / 2;
        } else {
            this.y = (this.a + vMargin) * lineTH + vMargin + this.a / 2;
        }
    }

    includesPoint(x, y) {
        return this.x - this.a / 2 < x && x < this.x + this.a / 2
            && this.y - this.a / 2 < y && y < this.y + this.a / 2;
    }

    mouseIn() {
        this.lastIn = Date.now();
    }

    draw(canvas, params) {
        let ctx = canvas.getContext('2d');
        let r = configuration.get('components.slot.color.r');
        let g = configuration.get('components.slot.color.g');
        let b = configuration.get('components.slot.color.b');
        ctx.strokeStyle = `rgb(${r}, ${g}, ${b})`;
        let alpha = 0.25;
        if (params.mouseIsIn) {
            // animation: go darker then lighter (0.25 -> 0.75 -> 0.25)
            let period = 2 * 1000;
            let totalTime = Date.now() - this.lastIn;
            let timeInCurrentPeriod = totalTime - parseInt(totalTime / period) * period;
            let percentage = timeInCurrentPeriod / period;
            let maxAlpha = 0.75;
            let minAlpha = 0.25;
            if (percentage > 0.5) {
                // going down
                percentage -= 0.5;
                alpha = maxAlpha - (maxAlpha - minAlpha) * percentage * 2;
            } else {
                // going up
                alpha = (maxAlpha - minAlpha) * percentage * 2 + minAlpha;
            }
        }
        ctx.fillStyle = `rgba(${r}, ${g}, ${b}, ${alpha})`;
        ctx.rect(this.x - this.a / 2, this.y - this.a / 2, this.a, this.a);
        ctx.fill();
        ctx.stroke();
        if (params.mouseIsIn) {
            ctx.beginPath();
            ctx.fillStyle = configuration.get('components.slot.fontColor');
            ctx.font = configuration.get('components.slot.font');
            ctx.textBaseline = 'middle';
            ctx.textAlign = 'center';
            ctx.fillText("" + (this.slotId + 1), this.x, this.y);
        }
    }

    onCardMoveEnd(card, pos) {
        if (this.includesPoint(pos.x, pos.y)) {
            card.mode = 'normal';
            this.game.broadcast('acceptCardMove', this, {card: card, x: this.x, y: this.y, fromX: pos.x, fromY: pos.y});
        }
    }
}

module.exports = CardSlot;
