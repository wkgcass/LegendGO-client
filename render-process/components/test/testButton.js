let configuration = require('../../../configuration');
let utils = require('../../../utils');
let cardSelectionService = require('../../inner-event-services/cardSelectionService');
let swal = require('sweetalert');

class TestButton {
    constructor(name, type, x, y) {
        this.name = name;
        this.type = type;
        this.x = x;
        this.y = y;
        this.width = configuration.get('components.testButton.width');
        this.height = configuration.get('components.testButton.height');
    }

    init(initConfig) {
        this.game = initConfig.game;
    }

    setTestDeckCards(cards) {
        this.testDeckCards = cards;
    }

    mouseDown() {
        let self = this;
        if (this.type == 'flip' || this.type == 'rotate' || this.type == 'rotate-and-flip') {
            cardSelectionService.start(null, 1, 1, function (cards) {
                let card = cards[0];
                card.onCardAnimation(null, {
                    compId: card.compId,
                    operation: self.type
                });
            });
        } else if (this.type == 'add-normal') {
            this.__askCrystal('add', 'normal');
        } else if (this.type == 'add-extra') {
            this.__askCrystal('add', 'extra');
        } else if (this.type == 'use-normal') {
            this.__askCrystal('use', 'normal');
        } else if (this.type == 'use-extra') {
            this.__askCrystal('use', 'extra');
        } else if (this.type == 'to-deck') {
            this.__cardMove('deck');
        } else if (this.type == 'to-ex') {
            this.__cardMove('ex');
        } else if (this.type == 'to-grave') {
            this.__cardMove('grave');
        } else if (this.type == 'to-out') {
            this.__cardMove('out');
        } else if (this.type == 'to-hand') {
            this.__cardMove('hand');
        } else if (this.type == 'change-hp') {
            this.game.broadcast('hpChange', this, {
                side: 'bottom',
                value: 2000
            });
        } else if (this.type == 'add-buff') {
            cardSelectionService.start(null, 1, 1, function (cards) {
                let card = cards[0];
                card.info.buff.push({
                    id: '1',
                    type: 'def-down',
                    value: 2000,
                    remains: 2
                });
            });
        } else if (this.type == 'remove-buff') {
            cardSelectionService.start(null, 1, 1, function (cards) {
                let card = cards[0];
                card.info.buff = [];
            });
        } else if (this.type == 'draw-card') {
            this.game.componentBroadcast('pileCardMove', this, {
                side: 'top',
                from: 'deck',
                to: 'hand',
                cards: this.testDeckCards
            });
        } else if (this.type == 'draw-card-to-grave') {
            this.game.componentBroadcast('pileCardMove', this, {
                side: 'top',
                from: 'deck',
                to: 'grave',
                cards: this.testDeckCards
            });
        } else if (this.type == 'grave-to-out') {
            this.game.componentBroadcast('pileCardMove', this, {
                side: 'top',
                from: 'grave',
                to: 'out',
                cards: this.testDeckCards,
                fromTop: this.testDeckCards[0],
                toTop: this.testDeckCards[0]
            });
        } else if (this.type == 'grave-to-hand') {
            this.game.componentBroadcast('pileCardMove', this, {
                side: 'top',
                from: 'grave',
                to: 'hand',
                cards: this.testDeckCards,
                fromTop: this.testDeckCards[0],
                toTop: this.testDeckCards[0]
            });
        }
    }

    __askCrystal(operation, type) {
        let self = this;
        swal({
            title: 'how many ' + type + ' crystals to ' + operation + '? (default 1)',
            type: 'input',
            showCancelButton: true,
            closeOnConfirm: false
        }, function (v) {
            let result = parseInt(v) || 1;
            if (result < 0) return;
            swal({
                title: 'which side? b for bottom | t for top',
                type: 'input',
                showCancelButton: true
            }, function (v) {
                if (v === 'b') {
                    v = 'bottom';
                } else {
                    v = 'top';
                }
                let route;
                if (operation == 'use') {
                    route = 'decrease';
                } else {
                    route = 'increase';
                }
                route += 'Crystal';
                self.game.componentBroadcast(route, self, {
                    type: type,
                    side: v,
                    count: result
                });
            });
        });
    }

    __cardMove(toWhere) {
        let self = this;
        cardSelectionService.start(null, 1, 1, function (cards) {
            let card = cards[0];
            if (toWhere === 'hand') {
                self.game.broadcast('cardMoveToHand', self, card);
            } else {
                self.game.broadcast('cardMoveToPile', self, {
                    card: card,
                    pile: toWhere
                });
            }
        });
    }

    draw(canvas) {
        let ctx = canvas.getContext('2d');
        utils.drawArcRec(ctx, this.x - this.width / 2, this.y - this.height / 2, this.width, this.height,
            configuration.get('components.testButton.arc'));
        ctx.fillStyle = 'white';
        ctx.strokeStyle = 'black';
        ctx.fill();
        ctx.stroke();
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        ctx.fillStyle = 'black';
        ctx.font = '' + 30 + 'px 微软雅黑';
        ctx.fillText(this.name, this.x, this.y);
    }
}

module.exports = TestButton;
