let test = module.exports;
let canvas;
test.init = function (initArgs) {
    canvas = initArgs.canvas;
};

test.listen = function (event, arg) {
    alert(arg);
    let ctx = canvas.getContext('2d');
    ctx.fillStyle = '#000000';
    ctx.fillRect(0, 0, canvas.width, canvas.height);
};
