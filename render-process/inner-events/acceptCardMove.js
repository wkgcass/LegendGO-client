let handler = module.exports;

let game;

handler.init = function (config) {
    game = config.game;
};

handler.listen = function (acceptor, pos) {
    let animation = {
        compId: pos.card.compId,
        operation: 'move',
        x: acceptor.x,
        y: acceptor.y,
        fromX: pos.fromX,
        fromY: pos.fromY
    };
    game.componentBroadcast('cardAnimation', this, animation);
};
