let handler = module.exports;
let service = require('../inner-event-services/handCardService');

let game;

handler.init = function (config) {
    game = config.game;
    service.init({
        game: game
    });
};

function getSide(card) {
    // TODO
    return 'bottom';
}

handler.listen = function (sender, card) {
    let side = getSide(card);
    service.addCard(side, [card]);
};
